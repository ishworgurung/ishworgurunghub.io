## Hello!

I do *Software. UNIX. Distributed.*

You can find me drinking coffee, walking or playing clown with my daughter depending on the time of day.
Come say hello if we meet in person. I won't bite.

In the past life, I've been many things such as Web Application developer, DevOps,Backend developer,
Information Security engineer/consultant and System administrator.In the past, I had the pleasure of making
friends from all walks of life - high to low, north,south, east to west and west to east.. Good times.

These days, the times have changed and for good effing reason; It's definitely gottenbetter.
I get paid to develop **amazing** software and work with a great team in the Sydney office.
Me. Humbled. Fortunate. Challenged. Excited.